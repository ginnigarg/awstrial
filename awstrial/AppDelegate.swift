//
//  AppDelegate.swift
//  awstrial
//
//  Created by Guneet Garg on 25/12/18.
//  Copyright © 2018 Ginni Garg. All rights reserved.
//

import UIKit
import AWSAppSync
import AWSMobileClient

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appSyncClient: AWSAppSyncClient?
    var awsSharedInstance = AWSMobileClient.init()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        print(checkTextSufficientComplexity("Abc@1234"))
        print(checkTextSufficientComplexity("abc@1234"))
        print(checkTextSufficientComplexity("Abc1234"))
        initializeAppSync()
        initializeMobileClient()
        return true
    }
    
    func checkTextSufficientComplexity(_ text : String) -> Bool{
        
        
        let capitalLetterRegEx  = ".*[A-Z]+.*"
        let texttest = NSPredicate(format:"SELF MATCHES %@", capitalLetterRegEx)
        let capitalresult = texttest.evaluate(with: text)
        print("\(capitalresult)")
        
        
        let numberRegEx  = ".*[0-9]+.*"
        let texttest1 = NSPredicate(format:"SELF MATCHES %@", numberRegEx)
        let numberresult = texttest1.evaluate(with: text)
        print("\(numberresult)")
        
        
        let specialCharacterRegEx  = ".*[!&^%$#@()/]+.*"
        let texttest2 = NSPredicate(format:"SELF MATCHES %@", specialCharacterRegEx)
        
        var specialresult = texttest2.evaluate(with: text)
        print("\(specialresult)")
        
        return capitalresult || numberresult || specialresult
        
    }

    
    
    func initializeMobileClient() {
        AWSMobileClient.sharedInstance().initialize { (userState, error) in
            if let userState = userState {
                print("UserState: \(userState.rawValue)")
            } else if let error = error {
                print("error: \(error.localizedDescription)")
            }
        }
        
        AWSMobileClient.sharedInstance().signUp(username: "abcde", password: "ABC@ab124", userAttributes: ["name": "abcde", "email": "abcde@gmail.com", "phone_number" : "+919599665803"]) { (signUpResult, error) in
            print("Sign Up Request Send")
            print(error)
            if let signUpResult = signUpResult {
                switch(signUpResult.signUpConfirmationState) {
                case .confirmed:
                    print("User is signed up and confirmed.")
                case .unconfirmed:
                    print("User is not confirmed and needs verification via \(signUpResult.codeDeliveryDetails!.deliveryMedium) sent at \(signUpResult.codeDeliveryDetails!.destination!)")
                case .unknown:
                    print("Unexpected case")
                }
            } else if let error = error {
                if let error = error as? AWSMobileClientError {
                    switch(error) {
                    case .usernameExists(let message):
                        print(message)
                    default:
                        break
                    }
                }
                print("\(error.localizedDescription)")
            }
        }
        
        AWSMobileClient.sharedInstance().getAWSCredentials { (credentials, error) in
            print(error)
            if let error = error {
                print("\(error.localizedDescription)")
            } else if let credentials = credentials {
                print(credentials.accessKey)
            }
        }
        
    }
    
    func initializeAppSync() {
        let databaseURL = URL(fileURLWithPath:NSTemporaryDirectory()).appendingPathComponent("awstrial")
        
        do {
            let appSyncConfig = try AWSAppSyncClientConfiguration(appSyncClientInfo: AWSAppSyncClientInfo(), userPoolsAuthProvider: {
                class MyCognitoUserPoolsAuthProvider : AWSCognitoUserPoolsAuthProviderAsync {
                    func getLatestAuthToken(_ callback: @escaping (String?, Error?) -> Void) {
                        AWSMobileClient.sharedInstance().getTokens { (tokens, error) in
                            if error != nil {
                                callback(nil, error)
                            } else {
                                callback(tokens?.idToken?.tokenString, nil)
                            }
                        }
                    }
                }
                return MyCognitoUserPoolsAuthProvider() }(),
            databaseURL:databaseURL)
            appSyncClient = try AWSAppSyncClient(appSyncConfig: appSyncConfig)
            print("Initialization done successfully")
        } catch {
            print("Error initializing appsync client. \(error)")
        }
        
    }

}

