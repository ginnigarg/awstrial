//
//  InitialViewController.swift
//  awstrial
//
//  Created by Guneet Garg on 25/12/18.
//  Copyright © 2018 Ginni Garg. All rights reserved.
//

import UIKit
import AWSAppSync
import AWSMobileClient

class InitialViewController: UIViewController {
    
    var appSyncClient: AWSAppSyncClient?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        appSyncClient = appDelegate.appSyncClient
    }
    
    
}
