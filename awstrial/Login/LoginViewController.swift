//
//  LoginViewController.swift
//  awstrial
//
//  Created by Guneet Garg on 25/12/18.
//  Copyright © 2018 Ginni Garg. All rights reserved.
//

import Foundation
import UIKit
import AWSAppSync
import AWSMobileClient

class LoginViewController: UIViewController {
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var pwdTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func loginPressed(_ sender: Any) {
        if emailTextField.text == "" || pwdTextField.text == "" {
            self.displayAlert(userMessage: "Please fill in your credentials")
            return
        }
        
        AWSMobileClient.sharedInstance().signIn(username: emailTextField.text!, password: pwdTextField.text!) { (signInResult, error) in
            if let error = error  {
                print("\(error.localizedDescription)")
            } else if let signInResult = signInResult {
                switch (signInResult.signInState) {
                case .signedIn:
                    print("User is signed in.")
                case .smsMFA:
                    print("SMS message sent to \(signInResult.codeDetails!.destination!)")
                default:
                    print("Sign In needs info which is not et supported.")
                }
            }
        }
        
        
        
    }
    
    @IBAction func registerPressed() {
        self.performSegue(withIdentifier: "register", sender: self)
    }
}

