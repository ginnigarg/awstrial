//
//  RegisterViewController.swift
//  awstrial
//
//  Created by Guneet Garg on 25/12/18.
//  Copyright © 2018 Ginni Garg. All rights reserved.
//

import Foundation
import UIKit
import AWSAppSync
import AWSMobileClient

class RegisterViewController: UIViewController {
    
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var pwdTextField: UITextField!
    @IBOutlet weak var cnfpwdTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func continuePressed(_ sender: Any) {
        if nameTextField.text == "" || phoneTextField.text == "" || emailTextField.text == "" || pwdTextField.text == "" || cnfpwdTextField.text == "" {
            self.displayAlert(userMessage: "All fields Compulsory!")
            return
        }
        if pwdTextField.text == cnfpwdTextField.text {
            RegistrationConstants.register.email = emailTextField.text
            RegistrationConstants.register.password = pwdTextField.text
            RegistrationConstants.register.phoneNumber = phoneTextField.text
            RegistrationConstants.register.name = nameTextField.text
            self.performSegue(withIdentifier: "markHome", sender: self)
            
        } else {
            pwdTextField.text = ""
            cnfpwdTextField.text = ""
            self.displayAlert(userMessage: "Passwords Do Not Match!")
            return
        }
    }
}

