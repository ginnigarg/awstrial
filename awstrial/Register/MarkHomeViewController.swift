//
//  MarkHomeViewController.swift
//  awstrial
//
//  Created by Guneet Garg on 27/12/18.
//  Copyright © 2018 Ginni Garg. All rights reserved.
//

import Foundation
import UIKit
import MapKit

class MarkHomeViewController: UIViewController, UIGestureRecognizerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    
    var coords = CLLocationCoordinate2D()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addGesture()
    }
    
    func addGesture() {
        let gesture = UILongPressGestureRecognizer (target: self , action : #selector(addAnnotation (_ : )))
        gesture.minimumPressDuration = 1
        mapView.addGestureRecognizer(gesture)
    }
    
    @objc func addAnnotation (_ sender  : UILongPressGestureRecognizer) {
        let location = sender.location(in : mapView)
        coords = mapView.convert(location, toCoordinateFrom: mapView)
        let annotation = MKPointAnnotation ()
        annotation.coordinate = coords
        mapView.addAnnotation (annotation)
    }
    
    @IBAction func continuePressed() {
        RegistrationConstants.register.homeLat = String(coords.latitude)
        RegistrationConstants.register.homeLong = String(coords.longitude)
        
        if RegistrationConstants.register.homeLat == "0.0" && RegistrationConstants.register.homeLong == "0.0" {
            self.displayAlert(userMessage: "Please mark your Home Address on the map!!")
            return
        }
        
        self.performSegue(withIdentifier: "markWork", sender: self)
    }
    
}
