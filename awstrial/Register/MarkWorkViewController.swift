//
//  MarkWorkViewController.swift
//  awstrial
//
//  Created by Guneet Garg on 27/12/18.
//  Copyright © 2018 Ginni Garg. All rights reserved.
//

import Foundation
import UIKit
import MapKit
import AWSMobileClient

class MarkWorkViewController: UIViewController, UIGestureRecognizerDelegate, MKMapViewDelegate {
    
    @IBOutlet weak var mapView: MKMapView!
    var awsSharedInstance = AWSMobileClient.init()
    
    var coords = CLLocationCoordinate2D()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        addGesture()
    }
    
    func addGesture() {
        let gesture = UILongPressGestureRecognizer (target: self , action : #selector(addAnnotation (_ : )))
        gesture.minimumPressDuration = 1
        mapView.addGestureRecognizer(gesture)
        
        
    }
    
    @objc func addAnnotation (_ sender  : UILongPressGestureRecognizer) {
        let location = sender.location(in : mapView)
        coords = mapView.convert(location, toCoordinateFrom: mapView)
        let annotation = MKPointAnnotation ()
        annotation.coordinate = coords
        mapView.addAnnotation (annotation)
    }
    
    @IBAction func register() {
        RegistrationConstants.register.workLat = String(coords.latitude)
        RegistrationConstants.register.workLong = String(coords.longitude)
        
        if RegistrationConstants.register.workLat == "0.0" && RegistrationConstants.register.workLong == "0.0" {
            self.displayAlert(userMessage: "Please mark your Work Address on the map!!")
            return
        }
        
        awsSharedInstance.signUp(username: RegistrationConstants.register.email!, password: RegistrationConstants.register.password!, userAttributes: ["email": RegistrationConstants.register.email!, "phone_number": RegistrationConstants.register.phoneNumber!, "homeLat": RegistrationConstants.register.homeLat!, "homeLong": RegistrationConstants.register.homeLong!, "workLat": RegistrationConstants.register.workLat!, "workLong": RegistrationConstants.register.workLong!]) { (signUpResult, error) in
            print("Sign Up Request Send")
            if let signUpResult = signUpResult {
                switch(signUpResult.signUpConfirmationState) {
                case .confirmed:
                    print("User is signed up and confirmed.")
                case .unconfirmed:
                    print("User is not confirmed and needs verification via \(signUpResult.codeDeliveryDetails!.deliveryMedium) sent at \(signUpResult.codeDeliveryDetails!.destination!)")
                case .unknown:
                    print("Unexpected case")
                }
            } else if let error = error {
                if let error = error as? AWSMobileClientError {
                    switch(error) {
                    case .usernameExists(let message):
                        print(message)
                    default:
                        break
                    }
                }
                print("\(error.localizedDescription)")
            }
        }
        
        self.displayAlert(userMessage: "Registration Successful")
        let viewController = storyboard?.instantiateViewController(withIdentifier: "LoginViewController")  as! LoginViewController
        self.present(viewController, animated: true, completion: nil)
        
        print(RegistrationConstants.register.email!)
        print(RegistrationConstants.register.password!)
        print(RegistrationConstants.register.name!)
        print(RegistrationConstants.register.phoneNumber!)
        print(RegistrationConstants.register.homeLat!)
        print(RegistrationConstants.register.homeLong!)
        print(RegistrationConstants.register.workLong!)
        print(RegistrationConstants.register.workLat!)
    }
    
    
}
