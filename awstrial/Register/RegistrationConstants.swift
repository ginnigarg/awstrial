//
//  RegistrationConstants.swift
//  awstrial
//
//  Created by Guneet Garg on 27/12/18.
//  Copyright © 2018 Ginni Garg. All rights reserved.
//

import Foundation


class RegistrationConstants {
    
    static var register = RegistrationConstants()
    
    var email: String?
    var password: String?
    var phoneNumber: String?
    var name: String?
    var homeLat: String?
    var homeLong: String?
    var workLat: String?
    var workLong: String?
}
